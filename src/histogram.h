#ifndef _histogram_h_
#define _histogram_h_
#include <vector>
#include <iostream>
#include "billing.h"

class Histogram
{
private:
	std::vector<float> dane_;
	mutable bool srednia_akt_;
	mutable bool odchylenie_akt_;
	mutable float srednia_;
	mutable float odchylenie_;

public:
	Histogram();
	void dodaj(float x);
	size_t rozmiar() const;
	float srednia() const;
	float odchylenie() const;
	float max() const;
	float min() const;
	int getCapacity() const;
};

#endif
