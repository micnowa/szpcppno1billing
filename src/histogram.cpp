#include "histogram.h"
#include "cmath"

/*
 std::vector<float> dane_;
 mutable bool srednia_akt_;
 mutable bool odchylenie_akt_;
 mutable float srednia_;
 mutable float odchylenie_;
 */

Histogram::Histogram()
{
	dane_.reserve(1);
	srednia_akt_ = false;
	odchylenie_akt_ = false;
	srednia_ = 0;
	odchylenie_ = 0;
}

void Histogram::dodaj(float x)
{
	dane_.push_back(x);
	srednia_akt_ = false;
	odchylenie_akt_ = false;
}

size_t Histogram::rozmiar() const
{
	return dane_.size();
}

float Histogram::srednia() const
{
	if (srednia_akt_ && odchylenie_akt_) return srednia_;
	srednia_ = 0;
	for (std::vector<float>::const_iterator it = dane_.begin(); it != dane_.end(); it++)
	{
		srednia_ += *it;
	}
	srednia_ = srednia_ / dane_.size();
	srednia_akt_ = true;
	return srednia_;
}

float Histogram::odchylenie() const
{
	if (srednia_akt_ && odchylenie_akt_) return odchylenie_;
	odchylenie_ = 0;
	float srednia_tmp = srednia();
	for (std::vector<float>::const_iterator it = dane_.begin(); it != dane_.end(); it++)
	{
		odchylenie_ += pow(*it, 2);
	}
	odchylenie_ = odchylenie_ / dane_.size();
	srednia_tmp = srednia_tmp * srednia_tmp;
	odchylenie_ = sqrt(odchylenie_ - srednia_tmp);
	odchylenie_akt_ = true;
	return odchylenie_;
}

float Histogram::max() const
{
	return *max_element (dane_.begin(), dane_.end());
}

float Histogram::min() const
{
	return *min_element (dane_.begin(), dane_.end());
}

int Histogram::getCapacity() const
{
	return dane_.size();
}
