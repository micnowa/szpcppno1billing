//============================================================================
// Name        : main.cpp
// Author      : Michał Nowaliński
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include <iostream>
#include <fstream>
#include <string>
#include "billing.h"
#include "histogram.h"

using namespace std;

int main()
{
	ifstream cin("dane.txt");
	Billing bil(cin);
	cout << "*** STATYSTYKA KRAJOWA ***" << endl;
	bil. statystykaKrajowa(cout);
	cout.precision(2);
	cout << "\n*** STATYSTYKA DZIENNA ***" << endl;
	bil. statystykaDzienna(cout);
	return 0;
}
