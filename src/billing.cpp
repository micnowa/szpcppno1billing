#include "billing.h"
#include "histogram.h"
#include <fstream>
#include <map>

std::string Polaczenie::kod() const
{
	std::string kod_tmp(nr, 0, 2);
	return kod_tmp;
}

std::istream &operator>>(std::istream &is, Polaczenie &p)
{
	/* Wczytywania danych z pliku */
	is >> p.dzien;
	is >> p.nr;
	is >> p.czas;

	return is;
}

Billing::Billing(std::istream &is)
{
	Polaczenie p;
	while (is.peek() != EOF)
	{
		is >> p;
		blng_.push_back(p);
	}
}

void Billing::statystykaDzienna(std::ostream &os) const
{
	std::vector<unsigned> stat(32, 0);

	for (std::vector<Polaczenie>::const_iterator it = blng_.begin(); it != blng_.end(); it++)
	{
		stat[(*it).dzien] += (*it).czas / 60;
	}

	int i = 1, sum_of_elems = 0;
	double percent = 0;
	for (std::vector<unsigned>::iterator it = stat.begin(); it != stat.end(); ++it)
		sum_of_elems += *it;

	for (std::vector<unsigned>::iterator it = stat.begin(); it != stat.end(); it++)
	{
		if (*it == 0) continue;
		percent = 100 * ((double) *it / sum_of_elems);
		os << i << ":" << "	";
		os << *it << " ";
		os << "(" << percent << "%): ";
		if (*it < 100) os << " ";
		for (int j = 0; j < (int) ((*it) / 20); j++)
			os << "*";
		os << std::endl;
		i++;
	}
	os << std::endl;
	os << "Wykonanych połączeń: " << blng_.size() << std::endl;

}

void Billing::statystykaKrajowa(std::ostream &os) const
{
	std::map<std::string, Histogram> stat;
	for (std::vector<Polaczenie>::const_iterator it = blng_.begin(); it != blng_.end(); it++)
	{
		stat[(*it).kod()].dodaj((*it).czas);
	}

	os << "Kraj	N	Sred.	Odch.	Min	Max" << std::endl;
	for (std::map<std::string, Histogram>::const_iterator it = stat.begin(); it != stat.end(); it++)
	{
		os	<< it->first <<"  	"<< it->second.getCapacity()<<"	"<<it->second.srednia()<<"	"<<it->second.odchylenie()<< "	"<<it->second.min()<< "	"<<it->second.max()<<std::endl;
	}

}

size_t Billing::getBillingNumber()
{
	return blng_.size();
}

std::vector<Polaczenie> Billing::getBilling()
{
	return blng_;
}
